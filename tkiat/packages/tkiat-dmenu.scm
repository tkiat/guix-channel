(define-module (tkiat packages tkiat-dmenu)
  #:use-module (gnu packages suckless)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module (tkiat packages))

(define-public tkiat-dmenu
  (package
    (inherit dmenu)
    (name "tkiat-dmenu")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
          (url "https://gitlab.com/tkiat/tkiat-dmenu.git")
          (commit "9cf5345d5fa211deb4ab8cc37553702467421e34")))
        (sha256
          (base32 "0wrr09440jphh3kwg4ssv8hqq54i0jbfmjrd4qg24by4i4sc8srb"))))
    (home-page "https://gitlab.com/tkiat/tkiat-dmenu")
    (synopsis "tkiat version of dmenu")))
