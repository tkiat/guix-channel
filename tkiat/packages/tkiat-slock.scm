(define-module (tkiat packages tkiat-slock)
  #:use-module (gnu packages suckless)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module (tkiat packages))

(define-public tkiat-slock
  (package
    (inherit slock)
    (name "tkiat-slock")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
          (url "https://gitlab.com/tkiat/tkiat-slock.git")
          (commit "07a907f67c2b5c5c7165fcb1ddfc174d1fa72e54")))
        (sha256
          (base32 "1975lw2hjdikzz0jp8ajzi0aa952dk3jc4vpv262yf6hljx71izh"))))
    (home-page "https://gitlab.com/tkiat/tkiat-slock")
    (synopsis "tkiat version of slock")))
