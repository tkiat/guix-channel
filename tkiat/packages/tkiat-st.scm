(define-module (tkiat packages tkiat-st)
  #:use-module (gnu packages suckless)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module (tkiat packages))

(define-public tkiat-st
  (package
    (inherit st)
    (name "tkiat-st")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
          (url "https://gitlab.com/tkiat/tkiat-st.git")
          (commit "47066fe464b57a01186880fc8da0c4b46a9780d6")))
        (sha256
          (base32 "0bflxn8hpjr9mhq3xg5a8hab26i67nlvzw24b49g388fmq2gxgff"))))
    (home-page "https://gitlab.com/tkiat/tkiat-st")
    (synopsis "tkiat version of st")))
