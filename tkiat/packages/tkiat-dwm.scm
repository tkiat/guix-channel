(define-module (tkiat packages tkiat-dwm)
  #:use-module (gnu packages suckless)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module (tkiat packages))

(define-public tkiat-dwm
  (package
    (inherit dwm)
    (name "tkiat-dwm")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
          (url "https://gitlab.com/tkiat/tkiat-dwm.git")
          (commit "0272aa13a3e60a7d4b7c3e9e79398d1e1bd998fe")))
        (sha256
          (base32 "1vf102rcslwkz6z72gz4w9xdhdgd1d1s9hm367q9lpcy5bp93w93"))))
    (home-page "https://gitlab.com/tkiat/tkiat-dwm")
    (synopsis "tkiat version of dwm")))
